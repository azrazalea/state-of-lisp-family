(TeX-add-style-hook
 "presentation"
 (lambda ()
   (setq TeX-command-extra-options
         "\"-shell-escape\"")
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("beamer" "bigger")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-environments-local "semiverbatim")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "beamer"
    "beamer10"
    "inputenc"
    "fontenc"
    "fixltx2e"
    "graphicx"
    "longtable"
    "float"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "marvosym"
    "wasysym"
    "amssymb"
    "hyperref")
   (LaTeX-add-labels
    "sec-1"
    "sec-1-1"
    "sec-1-2"
    "sec-1-3"
    "sec-2"
    "sec-2-1"
    "sec-2-2"
    "sec-2-3"
    "sec-2-4"
    "sec-2-5"
    "sec-2-6"
    "sec-2-7"
    "sec-2-7-1"
    "sec-2-7-2"
    "sec-2-8"
    "sec-2-8-1"
    "sec-2-8-2"
    "sec-2-9"
    "sec-3"
    "sec-3-1"
    "sec-3-2"
    "sec-3-3"
    "sec-3-4"
    "sec-3-5"
    "sec-3-6"
    "sec-4"
    "sec-4-1"
    "sec-4-2"
    "sec-4-3"
    "sec-4-4"
    "sec-5"
    "sec-5-1"
    "sec-5-2"
    "sec-5-3"
    "sec-5-4"
    "sec-6"
    "sec-6-1"
    "sec-6-2"
    "sec-6-3"
    "sec-6-4"
    "sec-7"
    "sec-7-1"
    "sec-7-2"
    "sec-7-3"
    "sec-7-4"
    "sec-8"
    "sec-8-1"
    "sec-8-2"
    "sec-9"
    "sec-9-1"
    "sec-9-2"
    "sec-10"
    "sec-10-1"
    "sec-10-2"
    "sec-11"
    "sec-11-1"
    "sec-11-2"
    "sec-11-3"
    "sec-11-4"))
 :latex)

